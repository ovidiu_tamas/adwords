<?php

/**
 * [readBudgetHistory description]
 * @param  string $fileName  [name of the file]
 * @param  string $startDate [start date as string]
 * @param  string $endDate   [end date as string]
 * @return array             [return an array]
 */
function readBudgetHistory( string $fileName, string $startDate, string $endDate ){
	$toReturn = [];

	if (file_exists($fileName)){
		$fileHandle = fopen($fileName, 'r');
		if ($fileHandle){
			$lineCounter = 0;
			while (!feof($fileHandle)) {
				$readLine = fgets($fileHandle);
				$dateTmp = substr($readLine, 0, strpos($readLine,":"));

				if (isDateInInterval($dateTmp, $startDate, $endDate)){
					//echo "Read line:".$dateTmp.PHP_EOL;									 						
					$budgetLine      = substr($readLine, strpos($readLine,":")+1);												
					$priceAndHours   = getDailyBudgetAndHours($budgetLine);
					$toReturn[]      = ["day"=>$dateTmp,"price_and_hours"=>$priceAndHours, "line_counter"=>$lineCounter];		
					$lineCounter++;
				}
			}
			fclose($fileHandle);
		}else{
			throw new Exception("File ($fileName) cannot be opened", 1);
		}
	}else{
		throw new Exception("The file ($fileName) cannot be found", 1);
	}
	return $toReturn;
}


/**
 * check if date is in interval
 * @param  string  $temporaryDate [description]
 * @param  string  $startDate     [description]
 * @param  string  $endDate       [description]
 * @return boolean                [description]
 */
function isDateInInterval( string $temporaryDate, string $startDate, string $endDate ){
	$toReturn      = false;	
	$temporaryDate = DateTime::createFromFormat('m.d.Y',$temporaryDate);
	$startDate     = DateTime::createFromFormat('m.d.Y',$startDate);
	$endDate       = DateTime::createFromFormat('m.d.Y',$endDate);

	if ( $temporaryDate !== false 
		&& $startDate !== false 
		&& $endDate !== false 
		&& $startDate<=$temporaryDate && $temporaryDate<=$endDate){
		$toReturn = true;
	}
	return $toReturn;
}

/**
 * the method get the daily budget based on hours
 * @param  string $line [description]
 * @throws Exceptopm    [description]
 * @return array        [description]
 */
function getDailyBudgetAndHours(string $line){
	$toReturn = [];
	//echo $line.PHP_EOL;
	preg_match_all('/((\d+)\s\((\d{2})\:(\d{2})\))/', $line, $matches);
	
	if (count($matches)!=5){
		throw new Exception("Sorry but I cannot read amounts and timestamps from the line ($line)", 1);
	}else{
		//print_r($matches);
		//we need to transfor evrething to minutes 
		//the price will be until minutes
		foreach ($matches[2] as $key => $price) {
			$toReturn[] = ["price"=>$price,"until_minutes"=>intval($matches[3][$key])*60+intval($matches[4][$key])];
		}
	}
	return $toReturn;	
}

/**
 * generate costs per day between dates
 * @param  array  $startDate [description]
 * @param  array  $endDate   [description]
 * @return [type]            [description]
 */
function generateCostPerDayBetweenDates( array $startDate, array $endDate ){
	$toReturn = [];

	$datesGeneration    = getDatesFromRange($startDate["day"],$endDate["day"]);
	$datesGeneration[0] = $startDate;
	$datesGeneration[count($datesGeneration)-1] = $endDate;
	
	$budget = getStartBudgetAndDailyBudget($startDate);	

	$budgetToGenerateCost = $startDate["line_counter"] == 0  ? $budget["start_budget"] : $budget["daily_budget"];
	//special case start date
	$toReturn[] = generateCostPerDay($startDate, $budgetToGenerateCost);
	
	if (count($datesGeneration)>=3){
		for($i=1;$i<count($datesGeneration)-1;$i++){
			$toReturn[] = generateCostBetweenStartAndEndDay( $datesGeneration[$i]["day"], $budget["daily_budget"] );
		}
	}

	return $toReturn;	
}

/**
 * generate cost for a day with intervals
 * @param  array  $currentDay     [description]
 * @param  int    $dailyBudget   [description]
* @return array                  [description]
 */
function generateCostPerDay( array $currentDay, int $dailyBudget ){
	$toReturn = [];

	$toReturn["day"] = $currentDay["day"];
	$toReturn["price_and_hours"] = [];
	$toReturn["budget"] = $dailyBudget;
	$dailyCost=0;

	//detect if first day
	if ($currentDay["line_counter"]==0){
		$counter = 0;

		while( $counter < count($currentDay["price_and_hours"])  && $dailyCost < $dailyBudget*2 ) {
			if ( $currentDay["price_and_hours"][$counter]["price"]==0){
				$counter++;
				//no need to generate hours for interval where price is 0
				continue;
			}
			$startHour = $currentDay["price_and_hours"][$counter]["until_minutes"];	
			if ( array_key_exists($counter+1,$currentDay["price_and_hours"])) {		
				$endHour = $currentDay["price_and_hours"][$counter+1]["until_minutes"];
			}else{
				$endHour = 23*60;
			}
			$counter++;
			$tempResult = generateCostsOnInterval(round($dailyBudget/count($currentDay["price_and_hours"])), $startHour, $endHour);
			foreach ($tempResult as $key => $value) {
				$toReturn["price_and_hours"][] = $value;
			}
			//get cost until now
			$dailyCost += checkDailyCostForIntervals($toReturn["price_and_hours"]);
		}
	}else{
		$counter = 0;
		while( $counter < count($currentDay["price_and_hours"])  && $dailyCost < $dailyBudget*2 ) {		
			//skyp this interval	
			if ( $currentDay["price_and_hours"][$counter]["price"]==0 && $counter>1){
				$counter++;
				//no need to generate hours for interval where price is 0
				continue;
			}
			if ($counter==0){
				$startHour = 0;//start from 00
			}else{
				$startHour = $currentDay["price_and_hours"][$counter]["until_minutes"];	
			}
			if ( array_key_exists($counter+1,$currentDay["price_and_hours"])) {		
				$endHour = $currentDay["price_and_hours"][$counter+1]["until_minutes"];
			}else{
				$endHour = 23*60;
			}
			$counter++;
			$tempResult = generateCostsOnInterval(round($dailyBudget/count($currentDay["price_and_hours"])), $startHour, $endHour);
			foreach ($tempResult as $key => $value) {
				$toReturn["price_and_hours"][] = $value;
			}	
			//get cost until now
			$dailyCost += checkDailyCostForIntervals($toReturn["price_and_hours"]);		
		}
	}

	return $toReturn;
}

/**
 * sum of daily cost
 * @param  [type] $intervals [description]
 * @return [type]            [description]
 */
function checkDailyCostForIntervals($intervals){
	$toReturn = 0;
	foreach ($intervals as $key => $value) {
		$toReturn += $value["price"];
	}
	return $toReturn;
}

/**
 * generate costs on interval
 * @param  int    $intervalBudget      [cost]
 * @param  int    $startHour [minutes]
 * @param  int    $endHour   [minutes]
 * @return float             [description]
 */
function generateCostsOnInterval( int $intervalBudget, int $startHour, int $endHour){
	$toReturn     = [];		
	$hour         = $startHour;
	$totalCost    = 0;

	if ($intervalBudget>0){
		//if in interval
		while($totalCost<2*$intervalBudget && $hour>=$startHour && $hour<$endHour){
			//try generate cost
			$generatedCost = randomNumberBetweenMinAndMax(0.1,$intervalBudget);				
			if($totalCost+$generatedCost<2*$intervalBudget){		
				$toReturn[] = ["price"=>$generatedCost,"until_minutes"=>rand($startHour,$endHour)];	

				//sum the value to total daily cost
				$totalCost+=$generatedCost;
				//go to next hour
				$hour+=60;
			}else{
				break;//no need to continue
			}
		}
	}else{
		$toReturn[] = ["price"=> 0];
	}	

	return $toReturn;
}

/**
 * get the start day budget and the rest of days budget
 * @param  array  $startDate [description]
 * @return array            [description]
 */
function getStartBudgetAndDailyBudget( array $startDate ){
	$toReturn = [];
	foreach ($startDate["price_and_hours"] as $key => $value) {
		//first pozitive
		if ($value["price"]>0){
			$toReturn["start_budget"] = $value["price"];
			break;
		}
	}
	//last value set in the day
	$toReturn["daily_budget"] = $startDate["price_and_hours"][count($startDate["price_and_hours"])-1]["price"];
	return $toReturn;
}
/** 
 * generate cost per day between dates
 * @param  int    $dailyBudget [description]
 * @return [type]              [description]
 */
function generateCostBetweenStartAndEndDay( string $day, int $dailyBudget ){	
	$toReturn  = [];
	$totalCost = 0;
	$hour      = 1;

	$toReturn["day"]    = $day;
	$toReturn["budget"] = $dailyBudget;
	//no intervals here we will generate some random intervals
	if ($dailyBudget>0){
		while($totalCost<2*$dailyBudget && $hour<23){
			$generatedCost = randomNumberBetweenMinAndMax(0.1,$dailyBudget);	
			if($totalCost+$generatedCost<2*$dailyBudget){		
				$toReturn["price_and_hours"][] = ["price"=>$generatedCost,"until_minutes"=>rand(($hour-1)*60,$hour*60)];	

				//sum the value to total daily cost
				$totalCost+=$generatedCost;
				//go to next hour
				$hour++;
			}else{
				break;//no need to continue
			}
		}
	}else{
		$toReturn["price_and_hours"]["price"] = 0;
	}
	return $toReturn;
}

/**
 * generate all days between two dates
 * @param  string $start [description]
 * @param  string $end   [description]
 * @return array         [description]
 */
function getDatesFromRange(string $start,string $end){
	$start = DateTime::createFromFormat('m.d.Y',$start)->format("d.m.Y");    
    $end   = DateTime::createFromFormat('m.d.Y',$end)->format("d.m.Y");
    $dates = [$start];
    while(strtotime(end($dates)) < strtotime($end)){
        $dates[] = date('d.m.Y', strtotime(end($dates).' +1 day'));
    }

    //because of the format
    foreach ($dates as $key => $value) {
    	$dates[$key] = ["day"=>date('m.d.Y',strtotime($value)),"price_and_hours"=>[]];
    }

    return $dates;
}

/**
 * random float number
 * @param  [type]  $st_num  [description]
 * @param  [type]  $end_num [description]
 * @param  integer $mul     [description]
 * @return [type]           [description]
 */
function randomNumberBetweenMinAndMax( $st_num, $end_num, $mul=10 ){
	if ($st_num>$end_num) {
		return false;
	}
	return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
}


/**
 * write the generated costs history
 * @param  array  $results [description]
 * @return void            [description]
 */
function writeGeneratedCosts( array $results ){
	$toReturn = "";
	foreach ($results as $key => $day) {		
		if (is_array($day) && array_key_exists("day", $day)){
			$rowResult = $day["day"].": ";
			foreach ($day["price_and_hours"] as $key => $interval) {
				if ($interval["price"]>0){
					$hour    = $interval["until_minutes"] > 60 ? (round($interval["until_minutes"]/60)<10 ? "0".round($interval["until_minutes"]/60) : round($interval["until_minutes"]/60) ) : "00";
					$minutes = $interval["until_minutes"] > 60 ? $interval["until_minutes"]%60 : $interval["until_minutes"];
					$rowResult .= $interval["price"]." (".$hour.":".$minutes."), ";
				}else{
					$rowResult .= 0;
				}
			}	
			$toReturn .= rtrim($rowResult,", ").";".PHP_EOL;					
		}
	}

	return $toReturn;
}

function writeDailyHistory( array $results ){
	$toReturn = "";
	foreach ($results as $key => $day) {		
		if (is_array($day) && array_key_exists("day", $day)){
			$rowResult = $day["day"]." ".$day["budget"]." ";
			$cost = 0;
			foreach ($day["price_and_hours"] as $key => $interval) {
				if ($interval["price"]>0){
					$cost+=	$interval["price"];
				}else{
					$cost = 0;
				}
			}	
			$rowResult .= $cost;
			$toReturn .= rtrim($rowResult,", ").";".PHP_EOL;					
		}
	}
	return $toReturn;
}


try{

	//parse the file into some proper data
	$dailyBudget   = readBudgetHistory("budget-history.txt","01.01.2019","03.01.2019");	
	$generatedCost = "";
	$history       = "";

	if (count($dailyBudget)){
		$counter = 0;
		//loop over the dates and try to generate the costs per each day
		while($counter<count($dailyBudget)){
			$results = [];
			$startDate = $dailyBudget[$counter];
			if( array_key_exists($counter+1, $dailyBudget)){
				$endDate = $dailyBudget[$counter+1];
			}else{
				$endDate = $dailyBudget[$counter];
			}
			$results        = generateCostPerDayBetweenDates($startDate, $endDate);
			$generatedCost .=  writeGeneratedCosts($results);	
			$history       .=  writeDailyHistory($results);
			$counter++;
		}
		
		echo $generatedCost;
		echo PHP_EOL;
		echo "Date |  Budget | Costs".PHP_EOL;
		echo $history;

	}

}catch(Exception $e){
	echo "Exception ".$e->getMessage().PHP_EOL;
}
?>
